
# Changelog

## Unreleased

- Improving design error reporting.
- Refactoring many design internal procedures.
- Collection is updatable in designers.

## [v0.6.0]

First release.
