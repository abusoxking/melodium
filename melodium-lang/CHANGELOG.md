
# Changelog

## Unreleased

- Improving error reporting.
- Refactoring script parsing process.
- Refactoring semantic build process.
- Restitution includes elements documentation.

## [v0.6.0]

First release.
