
# Mélodium core operations library

This library provides the basic mathematical operations as functions and treatments for the Mélodium environment, notably:
- arithmetic functions;
- trigonometric functions;
- comparison operations;
- logical operations.

## For Mélodium project

This library is made for use within the Mélodium environment and has no purpose for pure Rust projects.
Please refer to the [Mélodium Project](https://melodium.tech/) or
the [Mélodium crate](https://docs.rs/melodium/latest/melodium/) for more accurate and detailed information.
