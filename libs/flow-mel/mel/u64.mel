
use conv/u64::toVoid
use flow/void::count as voidCount

/**
    Gives count of elements passing through input stream.

    This count increment one for each element within the stream, starting at 1.
    
    ```mermaid
    graph LR
        T("count()")
        V["🟦 🟦 🟦 …"] -->|stream| T
        
        T -->|count| P["1️⃣ 2️⃣ 3️⃣ …"]
    
        style V fill:#ffff,stroke:#ffff
        style P fill:#ffff,stroke:#ffff
    ```
*/
treatment count()
  input stream: Stream<u64>
  output count: Stream<u128>
{
    toVoid()
    voidCount()

    Self.stream -> toVoid.value,iter -> voidCount.stream,count -> Self.count
}

/**
    Gives pattern of a `u64` stream.
    
    ```mermaid
    graph LR
        T("pattern()")
        A["… 🟨 🟨 🟨 🟨 🟨 🟨"] -->|stream| T
        
        T -->|pattern| O["… 🟦 🟦 🟦 🟦 🟦 🟦"]
    
        style A fill:#ffff,stroke:#ffff
        style O fill:#ffff,stroke:#ffff
*/
treatment pattern()
  input stream: Stream<u64>
  output pattern: Stream<void>
{
    toVoid()

    Self.stream -> toVoid.value,iter -> Self.pattern
}

