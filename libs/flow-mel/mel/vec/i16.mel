
use flow/vec/i16::pattern
use flow/vec/void::count as voidCount
use flow/vec/void::size as voidSize

/**
    Gives count of elements passing through stream.
    
    This count increment one for each vector within the stream, starting at 1.

    ℹ️ The count is independant from vector sizes.
    
    ```mermaid
    graph LR
        T("count()")
        V["［🟦 🟦］［🟦］［🟦 🟦 🟦］…"] -->|stream| T
        
        T -->|count| P["1️⃣ 2️⃣ 3️⃣ …"]
    
        style V fill:#ffff,stroke:#ffff
        style P fill:#ffff,stroke:#ffff
    ```
*/
treatment count()
  input stream: Stream<Vec<i16>>
  output count: Stream<u128>
{
    pattern()
    voidCount()

    Self.stream -> pattern.stream,pattern -> voidCount.stream,count -> Self.count
}

/**
    Gives size of vectors passing through stream.
    
    For each vector one `size` value is sent, giving the number of elements contained within matching vector.
    
    ```mermaid
    graph LR
        T("size()")
        V["［🟦 🟦］［🟦］［］［🟦 🟦 🟦］…"] -->|vector| T
        
        T -->|size| P["2️⃣ 1️⃣ 0️⃣ 3️⃣ …"]
    
        style V fill:#ffff,stroke:#ffff
        style P fill:#ffff,stroke:#ffff
    ```
*/
treatment size()
  input vector: Stream<Vec<i16>>
  output size: Stream<u64>
{
    pattern()
    voidSize()

    Self.vector -> pattern.stream,pattern -> voidSize.vector,size -> Self.size
}
