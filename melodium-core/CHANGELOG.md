
# Changelog

## Unreleased

- Adding `as_identified` call on elements.

## [v0.6.0]

First release.
