
# Changelog

## Unrelease

- Adding Status for better error management.
- Improving user display of elements.
- Improving LoadingError details.
- Buildable descriptors are able to tell if they depends on identified element.

## [v0.6.1]

- Adding parsing function to Identifier.

## [v0.6.0]

First release.
