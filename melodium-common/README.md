
# Mélodium common crate

Common Mélodium elements and traits.

This crate provides the base elements and traits used across the whole Mélodium environment.

Look at the [Mélodium crate](https://docs.rs/melodium/latest/melodium/)
or the [Mélodium Project](https://melodium.tech/) for more detailed information.
