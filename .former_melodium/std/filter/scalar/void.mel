use core/filter/scalar/void::Filter as CoreFilter

/**
Filter an input `void` stream according to `bool` stream.

ℹ️ If both streams are not the same size nothing is sent through accepted nor rejected.

```mermaid
graph LR
    T("Filter()")
    V["… 🟦 🟧 🟪 🟫 🟨 …"] -->|value| T
    D["… 🟩 🟥 🟥 🟩 🟥 …"] -->|decision|T
    
    T -->|accepted| A["… 🟦 🟫 …"]
    T -->|rejected| R["… 🟧 🟪 🟨 …"]

    style V fill:#ffff,stroke:#ffff
    style D fill:#ffff,stroke:#ffff
    style A fill:#ffff,stroke:#ffff
    style R fill:#ffff,stroke:#ffff
```
*/
sequence Filter()
  input  value:    Stream<void>
  input  decision: Stream<bool>
  output accepted: Stream<void>
  output rejected: Stream<void>
{
    CoreFilter()

    Self.value ----> CoreFilter.value,accepted ----> Self.accepted
    Self.decision -> CoreFilter.decision,rejected -> Self.rejected
}

