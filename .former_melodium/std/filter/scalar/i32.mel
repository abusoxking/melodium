use core/filter/scalar/i32::Filter as CoreFilter

/**
Filter an input `i32` stream according to `bool` stream.

ℹ️ If both streams are not the same size nothing is sent through accepted nor rejected.

```mermaid
graph LR
    T("Filter()")
    V["… 🟦 🟧 🟪 🟫 🟨 …"] -->|value| T
    D["… 🟩 🟥 🟥 🟩 🟥 …"] -->|decision|T
    
    T -->|accepted| A["… 🟦 🟫 …"]
    T -->|rejected| R["… 🟧 🟪 🟨 …"]

    style V fill:#ffff,stroke:#ffff
    style D fill:#ffff,stroke:#ffff
    style A fill:#ffff,stroke:#ffff
    style R fill:#ffff,stroke:#ffff
```
*/
sequence Filter()
  input  value:    Stream<i32>
  input  decision: Stream<bool>
  output accepted: Stream<i32>
  output rejected: Stream<i32>
{
    CoreFilter()

    Self.value ----> CoreFilter.value,accepted ----> Self.accepted
    Self.decision -> CoreFilter.decision,rejected -> Self.rejected
}

