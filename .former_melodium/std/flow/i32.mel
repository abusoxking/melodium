use core/linearize::LinearizeI32

/**
Linearize stream of `Vec<i32>` into stream of `Scalar<i32>`.

All the input vectors are turned into continuous stream of scalar values, in the same order.
```mermaid
graph LR
    T(Linearize)
    B["［🟦 🟦］［🟦］［🟦 🟦 🟦］"] -->|vector| T
    
    T -->|value| O["🟦 🟦 🟦 🟦 🟦 🟦"]

    style B fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```
*/
sequence Linearize()
  input vector: Stream<Vec<i32>>
  output value: Stream<i32>
{
    LinearizeI32()

    Self.vector -> LinearizeI32.vector,value -> Self.value
}


use core/flow::StreamI32ToBlockVecI32

/**
Transform whole stream of `i32` into a single vector block.

All the streamed values are added to a single vector, and once stream is over the vector is transmitted.

```mermaid
graph LR
    T(BlockAllStream)
    B["🟦 🟦 🟦 🟦 🟦 🟦"] -->|stream| T
    
    T -->|block| O["〈［🟦 🟦 🟦 🟦 🟦 🟦］〉"]

    style B fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```

⚠️ As the stream values are continuously inserted in vector and so kept in memory, user should be _really careful_ when using this treatment to not saturate memory.

*/
sequence BlockAllStream()
  input  stream: Stream<i32>
  output block:  Block<Vec<i32>>
{
    StreamI32ToBlockVecI32()

    Self.stream -> StreamI32ToBlockVecI32.data,data -> Self.block
}


use core/flow::BlockI32ToStream

/**
Stream a block of `i32`.

The received block is sent as streamed value, then the stream is closed.

```mermaid
graph LR
    T(StreamBlock)
    B["〈🟦〉"] -->|block| T
    
    T -->|stream| O["🟦"]

    style B fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```
*/
sequence StreamBlock()
  input  block:  Block<i32>
  output stream: Stream<i32>
{
    BlockI32ToStream()

    Self.block -> BlockI32ToStream.data,data -> Self.stream
}


use core/flow::BlockVecI32ToStream

/**
Stream a vector of `i32`.

The received block is sent as streamed vector, then the stream is closed.

```mermaid
graph LR
    T(StreamVecBlock)
    B["〈［🟦 🟦 🟦 🟦 🟦 🟦］〉"] -->|block| T
    
    T -->|stream| O["［🟦 🟦 🟦 🟦 🟦 🟦］"]

    style B fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```
*/
sequence StreamVecBlock()
  input  block:  Block<Vec<i32>>
  output stream: Stream<Vec<i32>>
{
    BlockVecI32ToStream()

    Self.block -> BlockVecI32ToStream.data,data -> Self.stream
}


use core/merge/scalar/i32::Merge as CoreMerge

/**
Merge two streams of `i32`.

The two streams are merged using the `order` stream:
- when `true`, value from `a` is used;
- when `false`, value from `b` is used.

ℹ️ No value from either `a` or `b` are discarded, they are used when `order` give turn.

⚠️ When `order` ends merge terminates without treating the remaining values from `a` and `b`.
When `order` give turn to `a` or `b` while the concerned stream is ended, the merge terminates.
Merge continues as long as `order` and concerned stream does, while the other can be ended.

```mermaid
graph LR
    T("Merge()")
    A["… 🟦 🟫 …"] -->|a| T
    B["… 🟧 🟪 🟨 …"] -->|b| T
    O["… 🟩 🟥 🟥 🟩 🟥 …"] -->|order|T
    

    T -->|value| V["… 🟦 🟧 🟪 🟫 🟨 …"]

    style V fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
    style A fill:#ffff,stroke:#ffff
    style B fill:#ffff,stroke:#ffff
```
*/
sequence Merge()
  input  a:     Stream<i32>
  input  b:     Stream<i32>
  input  order: Stream<bool>
  output value: Stream<i32>
{
    CoreMerge()

    Self.a -> CoreMerge.a
    Self.b -> CoreMerge.b
    Self.order -> CoreMerge.order,value -> Self.value
}


use core/merge/vector/i32::Merge as CoreVecMerge

/**
Merge two streams of `Vec<i32>`.

The two streams are merged using the `order` stream:
- when `true`, value from `a` is used;
- when `false`, value from `b` is used.

ℹ️ No value from either `a` or `b` are discarded, they are used when `order` give turn.

⚠️ When `order` ends merge terminates without treating the remaining values from `a` and `b`.
When `order` give turn to `a` or `b` while the concerned stream is ended, the merge terminates.
Merge continues as long as `order` and concerned stream does, while the other can be ended.

```mermaid
graph LR
    T("VecMerge()")
    A["… ［🟦 🟦］［🟫 🟫］…"] -->|a| T
    B["… ［🟧 🟧 🟧］［🟪］［🟨 🟨］…"] -->|b| T
    O["… 🟩 🟥 🟥 🟩 🟥 …"] -->|order|T
    

    T -->|value| V["…［🟦 🟦］［🟧 🟧 🟧］［🟪］［🟫 🟫］［🟨 🟨］…"]

    style V fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
    style A fill:#ffff,stroke:#ffff
    style B fill:#ffff,stroke:#ffff
```
*/
sequence VecMerge()
  input  a:     Stream<Vec<i32>>
  input  b:     Stream<Vec<i32>>
  input  order: Stream<bool>
  output value: Stream<Vec<i32>>
{
    CoreVecMerge()

    Self.a -> CoreVecMerge.a
    Self.b -> CoreVecMerge.b
    Self.order -> CoreVecMerge.order,value -> Self.value
}

use core/organize::OrganizeI32

/**
Organize stream of `i32` into stream of `Vec<i32>`.

ℹ️ If some remaining values doesn't fit into the pattern, they are trashed.
If there are not enough values to fit the pattern, uncomplete vector is trashed.

```mermaid
graph LR
    T(Organize)
    A["… 🟨 🟨 🟨 🟨 🟨 🟨"] -->|value| T
    B["[🟦 🟦] [🟦] [🟦 🟦 🟦]"] -->|pattern| T
    
    T -->|values| O["[🟨 🟨] [🟨] [🟨 🟨 🟨]"]

    style A fill:#ffff,stroke:#ffff
    style B fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```

*/
sequence Organize()
  input   value: Stream<i32>
  input pattern: Stream<Vec<void>>
  output values: Stream<Vec<i32>>
{
    OrganizeI32()

    Self.value ---> OrganizeI32.value,values -> Self.values
    Self.pattern -> OrganizeI32.pattern
}


use std/flow/void::Trigger as VoidTrigger
use std/conversion/scalar/i32::ToVoid

/**
Trigger on `i32` stream start and finish.

Send `start` when a first value is send through the stream.
Send `finish` when stream is finally over.

```mermaid
graph LR
    T(Trigger)
    B["🔴 … 🟦 🟦 🟦 🟦 🟦 🟦 … 🟢"] -->|value| T
    
    T -->|start| S["〈🟩〉"]
    T -->|finish| F["〈🟥〉"]

    style B fill:#ffff,stroke:#ffff
    style S fill:#ffff,stroke:#ffff
    style F fill:#ffff,stroke:#ffff
```

ℹ️ If the stream never receive any data before being closed, only `finish` will be emitted.
*/
sequence Trigger()
  input  value:  Stream<i32>
  output start:  Block<void>
  output finish: Block<void>
{
    ToVoid()
    VoidTrigger()

    Self.value -> ToVoid.value,iter -> VoidTrigger.iter
    VoidTrigger.start --> Self.start
    VoidTrigger.finish -> Self.finish
}


use std/flow/void::VecTrigger as VoidVecTrigger
use std/conversion/vector/i32::ToVoid as ToVecVoid

/**
Trigger on `Vec<i32>` stream start and finish.

Send `start` when a first vector is send through the stream.
Send `finish` when stream is finally over.

```mermaid
graph LR
    T(VecTrigger)
    B["🔴 …［🟦 🟦 🟦］［🟦 🟦 🟦］［🟦 🟦 🟦］［🟦 🟦 🟦］ … 🟢"] -->|value| T
    
    T -->|start| S["〈🟩〉"]
    T -->|finish| F["〈🟥〉"]

    style B fill:#ffff,stroke:#ffff
    style S fill:#ffff,stroke:#ffff
    style F fill:#ffff,stroke:#ffff
```

ℹ️ If the stream never receive any vector before being closed, only `finish` will be emitted.
*/
sequence VecTrigger()
  input  value:  Stream<Vec<i32>>
  output start:  Block<void>
  output finish: Block<void>
{
    ToVecVoid()
    VoidVecTrigger()

    Self.value -> ToVecVoid.vector,pattern -> VoidVecTrigger.iter
    VoidVecTrigger.start --> Self.start
    VoidVecTrigger.finish -> Self.finish
}


use std/flow/void::Count as VoidCount

/**
Gives count of elements passing through input stream.

This count increment one for each elements within the `value` stream, starting at 1.

```mermaid
graph LR
    T("Count()")
    V["🟦 🟦 🟦 …"] -->|value| T
    
    T -->|count| P["1️⃣ 2️⃣ 3️⃣ …"]

    style V fill:#ffff,stroke:#ffff
    style P fill:#ffff,stroke:#ffff
```
*/
sequence Count()
  input  value: Stream<i32>
  output count: Stream<u128>
{
    ToVoid()
    VoidCount()

    Self.value ->  ToVoid.value,iter -> VoidCount.iter,count -> Self.count
}


use std/flow/void::VecCount as VoidVecCount

/**
Gives count of elements passing through input stream.

This count increment one for each elements within the `value` stream, starting at 1.

ℹ️ This does not count the number of elements present in each vector, see the `Size` sequence instead.

```mermaid
graph LR
    T("VecCount()")
    V["［🟦 🟦］［🟦］［🟦 🟦 🟦］…"] -->|value| T
    
    T -->|count| P["1️⃣ 2️⃣ 3️⃣ …"]

    style V fill:#ffff,stroke:#ffff
    style P fill:#ffff,stroke:#ffff
```
*/
sequence VecCount()
  input  value: Stream<Vec<i32>>
  output count: Stream<u128>
{
    ToVecVoid()
    VoidVecCount()

    Self.value -> ToVecVoid.vector,pattern -> VoidVecCount.iter,count -> Self.count
}


use std/flow/void::Size as VoidSize

/**
Gives number of elements present in each vector passing through input stream.

For each vector one `size` value is sent, giving the number of elements contained within matching vector.

```mermaid
graph LR
    T("Size()")
    V["…［🟦 🟦］［🟦］［🟦 🟦 🟦］…"] -->|value| T
    
    T -->|size| P["… 2️⃣ 1️⃣ 3️⃣ …"]

    style V fill:#ffff,stroke:#ffff
    style P fill:#ffff,stroke:#ffff
```
*/
sequence Size()
  input  value: Stream<Vec<i32>>
  output size:  Stream<u64>
{
    ToVecVoid()
    VoidSize()

    Self.value -> ToVecVoid.vector,pattern -> VoidSize.pattern,size -> Self.size
}


use std/flow/void::Fit as VoidFit

/**
Creates stream of vectors based on requested sizes.

For each `size` received, a vector with the same number of values is sent through `pattern`.

```mermaid
graph LR
    T("Fit()")
    V["… 🟦 🟦 🟦 🟦 🟦 🟦 …"] -->|value| T
    S["… 2️⃣ 1️⃣ 3️⃣ …"] -->|size| T
    
    T -->|value| P["…［🟦 🟦］［🟦］［🟦 🟦 🟦］…"]

    style V fill:#ffff,stroke:#ffff
    style S fill:#ffff,stroke:#ffff
    style P fill:#ffff,stroke:#ffff
```
*/
sequence Fit()
  input  value: Stream<i32>
  input  size:  Stream<u64>
  output value: Stream<Vec<i32>>
{
    VoidFit()
    Organize()

    Self.size -> VoidFit.size,pattern -> Organize.pattern
    Self.value ------------------------> Organize.value,values -> Self.value
}

