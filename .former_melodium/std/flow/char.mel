use core/linearize::LinearizeChar

/**
Linearize stream of `Vec<char>` into stream of `Scalar<char>`.

All the input vectors are turned into continuous stream of scalar values, in the same order.
```mermaid
graph LR
    T(Linearize)
    B["［🟦 🟦］［🟦］［🟦 🟦 🟦］"] -->|vector| T
    
    T -->|value| O["🟦 🟦 🟦 🟦 🟦 🟦"]

    style B fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```
*/
sequence Linearize()
  input vector: Stream<Vec<char>>
  output value: Stream<char>
{
    LinearizeChar()

    Self.vector -> LinearizeChar.vector,value -> Self.value
}


use core/flow::StreamCharToBlockVecChar

/**
Transform whole stream of `char` into a single vector block.

All the streamed values are added to a single vector, and once stream is over the vector is transmitted.

```mermaid
graph LR
    T(BlockAllStream)
    B["🟦 🟦 🟦 🟦 🟦 🟦"] -->|stream| T
    
    T -->|block| O["〈［🟦 🟦 🟦 🟦 🟦 🟦］〉"]

    style B fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```

⚠️ As the stream values are continuously inserted in vector and so kept in memory, user should be _really careful_ when using this treatment to not saturate memory.

*/
sequence BlockAllStream()
  input  stream: Stream<char>
  output block:  Block<Vec<char>>
{
    StreamCharToBlockVecChar()

    Self.stream -> StreamCharToBlockVecChar.data,data -> Self.block
}


use core/flow::BlockCharToStream

/**
Stream a block of `char`.

The received block is sent as streamed value, then the stream is closed.

```mermaid
graph LR
    T(StreamBlock)
    B["〈🟦〉"] -->|block| T
    
    T -->|stream| O["🟦"]

    style B fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```
*/
sequence StreamBlock()
  input  block:  Block<char>
  output stream: Stream<char>
{
    BlockCharToStream()

    Self.block -> BlockCharToStream.data,data -> Self.stream
}


use core/flow::BlockVecCharToStream

/**
Stream a vector of `char`.

The received block is sent as streamed vector, then the stream is closed.

```mermaid
graph LR
    T(StreamVecBlock)
    B["〈［🟦 🟦 🟦 🟦 🟦 🟦］〉"] -->|block| T
    
    T -->|stream| O["［🟦 🟦 🟦 🟦 🟦 🟦］"]

    style B fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```
*/
sequence StreamVecBlock()
  input  block:  Block<Vec<char>>
  output stream: Stream<Vec<char>>
{
    BlockVecCharToStream()

    Self.block -> BlockVecCharToStream.data,data -> Self.stream
}


use core/merge/scalar/char::Merge as CoreMerge

/**
Merge two streams of `char`.

The two streams are merged using the `order` stream:
- when `true`, value from `a` is used;
- when `false`, value from `b` is used.

ℹ️ No value from either `a` or `b` are discarded, they are used when `order` give turn.

⚠️ When `order` ends merge terminates without treating the remaining values from `a` and `b`.
When `order` give turn to `a` or `b` while the concerned stream is ended, the merge terminates.
Merge continues as long as `order` and concerned stream does, while the other can be ended.

```mermaid
graph LR
    T("Merge()")
    A["… 🟦 🟫 …"] -->|a| T
    B["… 🟧 🟪 🟨 …"] -->|b| T
    O["… 🟩 🟥 🟥 🟩 🟥 …"] -->|order|T
    

    T -->|value| V["… 🟦 🟧 🟪 🟫 🟨 …"]

    style V fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
    style A fill:#ffff,stroke:#ffff
    style B fill:#ffff,stroke:#ffff
```
*/
sequence Merge()
  input  a:     Stream<char>
  input  b:     Stream<char>
  input  order: Stream<bool>
  output value: Stream<char>
{
    CoreMerge()

    Self.a -> CoreMerge.a
    Self.b -> CoreMerge.b
    Self.order -> CoreMerge.order,value -> Self.value
}


use core/merge/vector/char::Merge as CoreVecMerge

/**
Merge two streams of `Vec<char>`.

The two streams are merged using the `order` stream:
- when `true`, value from `a` is used;
- when `false`, value from `b` is used.

ℹ️ No value from either `a` or `b` are discarded, they are used when `order` give turn.

⚠️ When `order` ends merge terminates without treating the remaining values from `a` and `b`.
When `order` give turn to `a` or `b` while the concerned stream is ended, the merge terminates.
Merge continues as long as `order` and concerned stream does, while the other can be ended.

```mermaid
graph LR
    T("VecMerge()")
    A["… ［🟦 🟦］［🟫 🟫］…"] -->|a| T
    B["… ［🟧 🟧 🟧］［🟪］［🟨 🟨］…"] -->|b| T
    O["… 🟩 🟥 🟥 🟩 🟥 …"] -->|order|T
    

    T -->|value| V["…［🟦 🟦］［🟧 🟧 🟧］［🟪］［🟫 🟫］［🟨 🟨］…"]

    style V fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
    style A fill:#ffff,stroke:#ffff
    style B fill:#ffff,stroke:#ffff
```
*/
sequence VecMerge()
  input  a:     Stream<Vec<char>>
  input  b:     Stream<Vec<char>>
  input  order: Stream<bool>
  output value: Stream<Vec<char>>
{
    CoreVecMerge()

    Self.a -> CoreVecMerge.a
    Self.b -> CoreVecMerge.b
    Self.order -> CoreVecMerge.order,value -> Self.value
}

use core/organize::OrganizeChar

/**
Organize stream of `char` into stream of `Vec<char>`.

ℹ️ If some remaining values doesn't fit into the pattern, they are trashed.
If there are not enough values to fit the pattern, uncomplete vector is trashed.

```mermaid
graph LR
    T(Organize)
    A["… 🟨 🟨 🟨 🟨 🟨 🟨"] -->|value| T
    B["[🟦 🟦] [🟦] [🟦 🟦 🟦]"] -->|pattern| T
    
    T -->|values| O["[🟨 🟨] [🟨] [🟨 🟨 🟨]"]

    style A fill:#ffff,stroke:#ffff
    style B fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```

*/
sequence Organize()
  input   value: Stream<char>
  input pattern: Stream<Vec<void>>
  output values: Stream<Vec<char>>
{
    OrganizeChar()

    Self.value ---> OrganizeChar.value,values -> Self.values
    Self.pattern -> OrganizeChar.pattern
}


use std/flow/void::Trigger as VoidTrigger
use std/conversion/scalar/char::ToVoid

/**
Trigger on `char` stream start and finish.

Send `start` when a first value is send through the stream.
Send `finish` when stream is finally over.

```mermaid
graph LR
    T(Trigger)
    B["🔴 … 🟦 🟦 🟦 🟦 🟦 🟦 … 🟢"] -->|value| T
    
    T -->|start| S["〈🟩〉"]
    T -->|finish| F["〈🟥〉"]

    style B fill:#ffff,stroke:#ffff
    style S fill:#ffff,stroke:#ffff
    style F fill:#ffff,stroke:#ffff
```

ℹ️ If the stream never receive any data before being closed, only `finish` will be emitted.
*/
sequence Trigger()
  input  value:  Stream<char>
  output start:  Block<void>
  output finish: Block<void>
{
    ToVoid()
    VoidTrigger()

    Self.value -> ToVoid.value,iter -> VoidTrigger.iter
    VoidTrigger.start --> Self.start
    VoidTrigger.finish -> Self.finish
}


use std/flow/void::VecTrigger as VoidVecTrigger
use std/conversion/vector/char::ToVoid as ToVecVoid

/**
Trigger on `Vec<char>` stream start and finish.

Send `start` when a first vector is send through the stream.
Send `finish` when stream is finally over.

```mermaid
graph LR
    T(VecTrigger)
    B["🔴 …［🟦 🟦 🟦］［🟦 🟦 🟦］［🟦 🟦 🟦］［🟦 🟦 🟦］ … 🟢"] -->|value| T
    
    T -->|start| S["〈🟩〉"]
    T -->|finish| F["〈🟥〉"]

    style B fill:#ffff,stroke:#ffff
    style S fill:#ffff,stroke:#ffff
    style F fill:#ffff,stroke:#ffff
```

ℹ️ If the stream never receive any vector before being closed, only `finish` will be emitted.
*/
sequence VecTrigger()
  input  value:  Stream<Vec<char>>
  output start:  Block<void>
  output finish: Block<void>
{
    ToVecVoid()
    VoidVecTrigger()

    Self.value -> ToVecVoid.vector,pattern -> VoidVecTrigger.iter
    VoidVecTrigger.start --> Self.start
    VoidVecTrigger.finish -> Self.finish
}


use std/flow/void::Count as VoidCount

/**
Gives count of elements passing through input stream.

This count increment one for each elements within the `value` stream, starting at 1.

```mermaid
graph LR
    T("Count()")
    V["🟦 🟦 🟦 …"] -->|value| T
    
    T -->|count| P["1️⃣ 2️⃣ 3️⃣ …"]

    style V fill:#ffff,stroke:#ffff
    style P fill:#ffff,stroke:#ffff
```
*/
sequence Count()
  input  value: Stream<char>
  output count: Stream<u128>
{
    ToVoid()
    VoidCount()

    Self.value ->  ToVoid.value,iter -> VoidCount.iter,count -> Self.count
}


use std/flow/void::VecCount as VoidVecCount

/**
Gives count of elements passing through input stream.

This count increment one for each elements within the `value` stream, starting at 1.

ℹ️ This does not count the number of elements present in each vector, see the `Size` sequence instead.

```mermaid
graph LR
    T("VecCount()")
    V["［🟦 🟦］［🟦］［🟦 🟦 🟦］…"] -->|value| T
    
    T -->|count| P["1️⃣ 2️⃣ 3️⃣ …"]

    style V fill:#ffff,stroke:#ffff
    style P fill:#ffff,stroke:#ffff
```
*/
sequence VecCount()
  input  value: Stream<Vec<char>>
  output count: Stream<u128>
{
    ToVecVoid()
    VoidVecCount()

    Self.value -> ToVecVoid.vector,pattern -> VoidVecCount.iter,count -> Self.count
}


use std/flow/void::Size as VoidSize

/**
Gives number of elements present in each vector passing through input stream.

For each vector one `size` value is sent, giving the number of elements contained within matching vector.

```mermaid
graph LR
    T("Size()")
    V["…［🟦 🟦］［🟦］［🟦 🟦 🟦］…"] -->|value| T
    
    T -->|size| P["… 2️⃣ 1️⃣ 3️⃣ …"]

    style V fill:#ffff,stroke:#ffff
    style P fill:#ffff,stroke:#ffff
```
*/
sequence Size()
  input  value: Stream<Vec<char>>
  output size:  Stream<u64>
{
    ToVecVoid()
    VoidSize()

    Self.value -> ToVecVoid.vector,pattern -> VoidSize.pattern,size -> Self.size
}


use std/flow/void::Fit as VoidFit

/**
Creates stream of vectors based on requested sizes.

For each `size` received, a vector with the same number of values is sent through `pattern`.

```mermaid
graph LR
    T("Fit()")
    V["… 🟦 🟦 🟦 🟦 🟦 🟦 …"] -->|value| T
    S["… 2️⃣ 1️⃣ 3️⃣ …"] -->|size| T
    
    T -->|value| P["…［🟦 🟦］［🟦］［🟦 🟦 🟦］…"]

    style V fill:#ffff,stroke:#ffff
    style S fill:#ffff,stroke:#ffff
    style P fill:#ffff,stroke:#ffff
```
*/
sequence Fit()
  input  value: Stream<char>
  input  size:  Stream<u64>
  output value: Stream<Vec<char>>
{
    VoidFit()
    Organize()

    Self.size -> VoidFit.size,pattern -> Organize.pattern
    Self.value ------------------------> Organize.value,values -> Self.value
}

