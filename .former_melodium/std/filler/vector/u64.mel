use std/flow/void::StreamBlock
use std/filler/scalar/u64::StaticVecFill

/**
Emit a value as stream when trigger is received.

The vector is sent through the stream and the stream is closed immediately afterwards.

```mermaid
graph LR
    T("Emit(value=［🟧 🟧 🟧］)")
    B["〈🟦〉"] -->|trigger| T
    
    T -->|value| O["［🟧 🟧 🟧］"]

    style B fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```
*/
sequence Emit(var value: Vec<u64> = [0])
  input trigger: Block<void>
  output value:  Stream<Vec<u64>>
{
    StreamBlock()
    StaticVecFill(value=value)

    Self.trigger -> StreamBlock.block,stream -> StaticVecFill.pattern,value -> Self.value
}


use std/flow/void::Linearize
use std/flow/u64::Organize
use std/filler/scalar/u64::StaticFill as ScalarStaticFill

/**
Fill an input `Vec<void>` stream with scalar static `u64` values.

```mermaid
graph LR
    T("StaticFill (value=🟧)")
    B["…［🟦 🟦 🟦］［🟦 🟦］［🟦 🟦 🟦］…"] -->|pattern| T
    
    T -->|value| O["…［🟧 🟧 🟧］［🟧 🟧］［🟧 🟧 🟧］…"]

    style B fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```
*/
sequence StaticFill(var value: u64 = 0)
  input pattern: Stream<Vec<void>>
  output value:  Stream<Vec<u64>>
{
    Linearize()
    ScalarStaticFill(value=value)
    Organize()

    Self.pattern -> Linearize.vector,value -> ScalarStaticFill.pattern,value -> Organize.value,values -> Self.value
    Self.pattern -------------------------------------------------------------> Organize.pattern
    
}

use core/filling/vector::FillVecU64 as CoreFill

/**
Fill an input stream of `void` vectors with matching values from `u64` stream.

```mermaid
graph LR
    T("Fill()")
    V["… 🟥 🟧 🟨 …"] -->|value| T
    P["…［🟦 🟦 🟦］［🟦 🟦］［🟦 🟦 🟦］…"] -->|pattern| T
    
    T -->|value| O["…［🟥 🟥 🟥］［🟧 🟧］［🟨 🟨 🟨］…"]

    style V fill:#ffff,stroke:#ffff
    style P fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```
*/
sequence Fill()
  input value:   Stream<u64>
  input pattern: Stream<Vec<void>>
  output value:  Stream<Vec<u64>>
{
    CoreFill()

    Self.value ---> CoreFill.value
    Self.pattern -> CoreFill.pattern,value -> Self.value
}

