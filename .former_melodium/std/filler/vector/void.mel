use std/flow/void::StreamBlock
use std/filler/scalar/void::StaticVecFill

/**
Emit a value as stream when trigger is received.

The vector is sent through the stream and the stream is closed immediately afterwards.

```mermaid
graph LR
    T("Emit(value=［🟧 🟧 🟧］)")
    B["〈🟦〉"] -->|trigger| T
    
    T -->|value| O["［🟧 🟧 🟧］"]

    style B fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```
*/
sequence Emit(var value: Vec<void> = [0])
  input trigger: Block<void>
  output value:  Stream<Vec<void>>
{
    StreamBlock()
    StaticVecFill(value=value)

    Self.trigger -> StreamBlock.block,stream -> StaticVecFill.pattern,value -> Self.value
}

