use std/flow/void::StreamBlock
use std/filler/scalar/string::StaticVecFill

/**
Emit a value as stream when trigger is received.

The vector is sent through the stream and the stream is closed immediately afterwards.

```mermaid
graph LR
    T("Emit(value=［🟧 🟧 🟧］)")
    B["〈🟦〉"] -->|trigger| T
    
    T -->|value| O["［🟧 🟧 🟧］"]

    style B fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```
*/
sequence Emit(var value: Vec<string> = [""])
  input trigger: Block<void>
  output value:  Stream<Vec<string>>
{
    StreamBlock()
    StaticVecFill(value=value)

    Self.trigger -> StreamBlock.block,stream -> StaticVecFill.pattern,value -> Self.value
}


use std/flow/void::Linearize
use std/flow/string::Organize
use std/filler/scalar/string::StaticFill as ScalarStaticFill

/**
Fill an input `Vec<void>` stream with scalar static `string` values.

```mermaid
graph LR
    T("StaticFill (value=🟧)")
    B["…［🟦 🟦 🟦］［🟦 🟦］［🟦 🟦 🟦］…"] -->|pattern| T
    
    T -->|value| O["…［🟧 🟧 🟧］［🟧 🟧］［🟧 🟧 🟧］…"]

    style B fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```
*/
sequence StaticFill(var value: string = "")
  input pattern: Stream<Vec<void>>
  output value:  Stream<Vec<string>>
{
    Linearize()
    ScalarStaticFill(value=value)
    Organize()

    Self.pattern -> Linearize.vector,value -> ScalarStaticFill.pattern,value -> Organize.value,values -> Self.value
    Self.pattern -------------------------------------------------------------> Organize.pattern
    
}

use core/filling/vector::FillVecString as CoreFill

/**
Fill an input stream of `void` vectors with matching values from `string` stream.

```mermaid
graph LR
    T("Fill()")
    V["… 🟥 🟧 🟨 …"] -->|value| T
    P["…［🟦 🟦 🟦］［🟦 🟦］［🟦 🟦 🟦］…"] -->|pattern| T
    
    T -->|value| O["…［🟥 🟥 🟥］［🟧 🟧］［🟨 🟨 🟨］…"]

    style V fill:#ffff,stroke:#ffff
    style P fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```
*/
sequence Fill()
  input value:   Stream<string>
  input pattern: Stream<Vec<void>>
  output value:  Stream<Vec<string>>
{
    CoreFill()

    Self.value ---> CoreFill.value
    Self.pattern -> CoreFill.pattern,value -> Self.value
}

