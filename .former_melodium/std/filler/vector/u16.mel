use std/flow/void::StreamBlock
use std/filler/scalar/u16::StaticVecFill

/**
Emit a value as stream when trigger is received.

The vector is sent through the stream and the stream is closed immediately afterwards.

```mermaid
graph LR
    T("Emit(value=［🟧 🟧 🟧］)")
    B["〈🟦〉"] -->|trigger| T
    
    T -->|value| O["［🟧 🟧 🟧］"]

    style B fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```
*/
sequence Emit(var value: Vec<u16> = [0])
  input trigger: Block<void>
  output value:  Stream<Vec<u16>>
{
    StreamBlock()
    StaticVecFill(value=value)

    Self.trigger -> StreamBlock.block,stream -> StaticVecFill.pattern,value -> Self.value
}


use std/flow/void::Linearize
use std/flow/u16::Organize
use std/filler/scalar/u16::StaticFill as ScalarStaticFill

/**
Fill an input `Vec<void>` stream with scalar static `u16` values.

```mermaid
graph LR
    T("StaticFill (value=🟧)")
    B["…［🟦 🟦 🟦］［🟦 🟦］［🟦 🟦 🟦］…"] -->|pattern| T
    
    T -->|value| O["…［🟧 🟧 🟧］［🟧 🟧］［🟧 🟧 🟧］…"]

    style B fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```
*/
sequence StaticFill(var value: u16 = 0)
  input pattern: Stream<Vec<void>>
  output value:  Stream<Vec<u16>>
{
    Linearize()
    ScalarStaticFill(value=value)
    Organize()

    Self.pattern -> Linearize.vector,value -> ScalarStaticFill.pattern,value -> Organize.value,values -> Self.value
    Self.pattern -------------------------------------------------------------> Organize.pattern
    
}

use core/filling/vector::FillVecU16 as CoreFill

/**
Fill an input stream of `void` vectors with matching values from `u16` stream.

```mermaid
graph LR
    T("Fill()")
    V["… 🟥 🟧 🟨 …"] -->|value| T
    P["…［🟦 🟦 🟦］［🟦 🟦］［🟦 🟦 🟦］…"] -->|pattern| T
    
    T -->|value| O["…［🟥 🟥 🟥］［🟧 🟧］［🟨 🟨 🟨］…"]

    style V fill:#ffff,stroke:#ffff
    style P fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```
*/
sequence Fill()
  input value:   Stream<u16>
  input pattern: Stream<Vec<void>>
  output value:  Stream<Vec<u16>>
{
    CoreFill()

    Self.value ---> CoreFill.value
    Self.pattern -> CoreFill.pattern,value -> Self.value
}

