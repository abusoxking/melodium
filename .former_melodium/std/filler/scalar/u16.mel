use core/filling/scalar::StaticVecBlockU16

/**
Fill a triggering `void` block with vector static `u16` value.

```mermaid
graph LR
    T("StaticVecBlockFill (value=［🟧 🟧 🟧］)")
    B["〈🟦〉"] -->|trigger| T
    
    T -->|value| O["〈［🟧 🟧 🟧］〉"]

    style B fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```
*/
sequence StaticVecBlockFill(var value: Vec<u16>)
  input trigger: Block<void>
  output value:  Block<Vec<u16>>
{
    StaticVecBlockU16(value=value)

    Self.trigger -> StaticVecBlockU16.trigger,value -> Self.value
}


use core/filling/scalar::StaticVecU16

/**
Fill an input `void` stream with vector static `u16` values.

```mermaid
graph LR
    T("StaticVecFill (value=［🟧 🟧 🟧］)")
    B["… 🟦 🟦 🟦 …"] -->|pattern| T
    
    T -->|value| O["… ［🟧 🟧 🟧］［🟧 🟧 🟧］［🟧 🟧 🟧］ …"]

    style B fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```
*/
sequence StaticVecFill(var value: Vec<u16>)
  input pattern: Stream<void>
  output value:  Stream<Vec<u16>>
{
    StaticVecU16(value=value)

    Self.pattern -> StaticVecU16.pattern,value -> Self.value
}

use core/filling/scalar::StaticBlockU16

/**
Fill a triggering `void` block with a scalar static `u16` value.

```mermaid
graph LR
    T("StaticBlockFill (value=🟧)")
    B["〈🟦〉"] -->|trigger| T
    
    T -->|value| O["〈🟧〉"]

    style B fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```
*/
sequence StaticBlockFill(var value: u16 = 0)
  input trigger: Block<void>
  output value:  Block<u16>
{
    StaticBlockU16(value=value)

    Self.trigger -> StaticBlockU16.trigger,value -> Self.value
}


use core/filling/scalar::StaticU16

/**
Fill an input `void` stream with scalar static `u16` values.

```mermaid
graph LR
    T("StaticFill (value=🟧)")
    B["… 🟦 🟦 🟦 …"] -->|pattern| T
    
    T -->|value| O["… 🟧 🟧 🟧 …"]

    style B fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```
*/
sequence StaticFill(var value: u16 = 0)
  input pattern: Stream<void>
  output value:  Stream<u16>
{
    StaticU16(value=value)

    Self.pattern -> StaticU16.pattern,value -> Self.value
}


use std/flow/void::StreamBlock

/**
Emit a value as stream when trigger is received.

The value is sent through the stream and the stream is closed immediately afterwards.

```mermaid
graph LR
    T("Emit(value=🟧)")
    B["〈🟦〉"] -->|trigger| T
    
    T -->|value| O["🟧"]

    style B fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```
*/
sequence Emit(var value: u16 = 0)
  input trigger: Block<void>
  output value:  Stream<u16>
{
    StreamBlock()
    StaticFill(value=value)

    Self.trigger -> StreamBlock.block,stream -> StaticFill.pattern,value -> Self.value
}

