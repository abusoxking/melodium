use core/filling/scalar::StaticVecBlockU32

/**
Fill a triggering `void` block with vector static `u32` value.

```mermaid
graph LR
    T("StaticVecBlockFill (value=［🟧 🟧 🟧］)")
    B["〈🟦〉"] -->|trigger| T
    
    T -->|value| O["〈［🟧 🟧 🟧］〉"]

    style B fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```
*/
sequence StaticVecBlockFill(var value: Vec<u32>)
  input trigger: Block<void>
  output value:  Block<Vec<u32>>
{
    StaticVecBlockU32(value=value)

    Self.trigger -> StaticVecBlockU32.trigger,value -> Self.value
}


use core/filling/scalar::StaticVecU32

/**
Fill an input `void` stream with vector static `u32` values.

```mermaid
graph LR
    T("StaticVecFill (value=［🟧 🟧 🟧］)")
    B["… 🟦 🟦 🟦 …"] -->|pattern| T
    
    T -->|value| O["… ［🟧 🟧 🟧］［🟧 🟧 🟧］［🟧 🟧 🟧］ …"]

    style B fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```
*/
sequence StaticVecFill(var value: Vec<u32>)
  input pattern: Stream<void>
  output value:  Stream<Vec<u32>>
{
    StaticVecU32(value=value)

    Self.pattern -> StaticVecU32.pattern,value -> Self.value
}

use core/filling/scalar::StaticBlockU32

/**
Fill a triggering `void` block with a scalar static `u32` value.

```mermaid
graph LR
    T("StaticBlockFill (value=🟧)")
    B["〈🟦〉"] -->|trigger| T
    
    T -->|value| O["〈🟧〉"]

    style B fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```
*/
sequence StaticBlockFill(var value: u32 = 0)
  input trigger: Block<void>
  output value:  Block<u32>
{
    StaticBlockU32(value=value)

    Self.trigger -> StaticBlockU32.trigger,value -> Self.value
}


use core/filling/scalar::StaticU32

/**
Fill an input `void` stream with scalar static `u32` values.

```mermaid
graph LR
    T("StaticFill (value=🟧)")
    B["… 🟦 🟦 🟦 …"] -->|pattern| T
    
    T -->|value| O["… 🟧 🟧 🟧 …"]

    style B fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```
*/
sequence StaticFill(var value: u32 = 0)
  input pattern: Stream<void>
  output value:  Stream<u32>
{
    StaticU32(value=value)

    Self.pattern -> StaticU32.pattern,value -> Self.value
}


use std/flow/void::StreamBlock

/**
Emit a value as stream when trigger is received.

The value is sent through the stream and the stream is closed immediately afterwards.

```mermaid
graph LR
    T("Emit(value=🟧)")
    B["〈🟦〉"] -->|trigger| T
    
    T -->|value| O["🟧"]

    style B fill:#ffff,stroke:#ffff
    style O fill:#ffff,stroke:#ffff
```
*/
sequence Emit(var value: u32 = 0)
  input trigger: Block<void>
  output value:  Stream<u32>
{
    StreamBlock()
    StaticFill(value=value)

    Self.trigger -> StreamBlock.block,stream -> StaticFill.pattern,value -> Self.value
}

