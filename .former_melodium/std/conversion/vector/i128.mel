use std/flow/i128::Linearize

use core/conversion/vector::I128ToVoid

/**
Convert stream of `Vec<i128>` into `Vec<void>` one.

This conversion is useful to extract pattern from a stream of vectors and work on it.

*/
sequence ToVoid()
  input  vector:  Stream<Vec<i128>>
  output pattern: Stream<Vec<void>>
{
    I128ToVoid()

    Self.vector -> I128ToVoid.vector,pattern -> Self.pattern
}

use std/conversion/scalar/i128::ToString as ScalToString
use std/flow/string::Organize as OrganizeString

/**
Convert stream of `Vec<i128>` into `Vec<string>` one.

`Vec<i128>` gets converted into `Vec<string>`, and the resulting vectors are send through stream in continuity.
*/
sequence ToString()
  input  value: Stream<Vec<i128>>
  output value: Stream<Vec<string>>
{
    ToVoid()
    Linearize()
    ScalToString()
    OrganizeString()

    Self.value -> Linearize.vector,value -> ScalToString.value,value -> OrganizeString.value,values -> Self.value
    Self.value -> ToVoid.vector,pattern ------------------------------> OrganizeString.pattern
}

use std/conversion/scalar/i128::ToF32 as ScalToF32
use std/flow/f32::Organize as OrganizeF32

/**
Convert stream of `Vec<i128>` into `Vec<f32>` one.

`Vec<i128>` gets converted into `Vec<f32>`, and the resulting vectors are send through stream in continuity.
This conversion is lossless, `i128` values can all fit into `f32`.
*/
sequence ToF32()
  input  value: Stream<Vec<i128>>
  output value: Stream<Vec<f32>>
{
    ToVoid()
    Linearize()
    ScalToF32()
    OrganizeF32()

    Self.value -> Linearize.vector,value -> ScalToF32.value,value -> OrganizeF32.value,values -> Self.data
    Self.value -> ToVoid.vector,pattern -----------------------------> OrganizeF32.pattern
}

use std/conversion/scalar/i128::ToF64 as ScalToF64
use std/flow/f64::Organize as OrganizeF64

/**
Convert stream of `Vec<i128>` into `Vec<f64>` one.

`Vec<i128>` gets converted into `Vec<f64>`, and the resulting vectors are send through stream in continuity.
This conversion is lossless, `i128` values can all fit into `f64`.
*/
sequence ToF64()
  input  value: Stream<Vec<i128>>
  output value: Stream<Vec<f64>>
{
    ToVoid()
    Linearize()
    ScalToF64()
    OrganizeF64()

    Self.value -> Linearize.vector,value -> ScalToF64.value,value -> OrganizeF64.value,values -> Self.data
    Self.value -> ToVoid.vector,pattern -----------------------------> OrganizeF64.pattern
}

use std/conversion/scalar/i128::ToU8 as ScalToU8
use std/flow/u8::Organize as OrganizeU8

/**
Convert stream of `Vec<i128>` into `Vec<u8>` one.

`Vec<i128>` gets converted into `Vec<u8>`, and the resulting vectors are send through stream in continuity.

As this conversion might be lossy (every possible `i128` value cannot fit into `u8`),
`truncate` allows value to be truncated to fit into a `u8`, and `or_default` set the
value that is assigned when a `i128` is out of range for `u8` and truncation not allowed.

Truncation happens on the binary level, thus: `10010110` (150 if unsigned, -106 if [signed](https://en.wikipedia.org/wiki/Signed_number_representations)) → `0110` (6).
*/
sequence ToU8(var truncate: bool = true, var or_default: u8 = 0)
  input  value: Stream<Vec<i128>>
  output value: Stream<Vec<u8>>
{
    ToVoid()
    Linearize()
    ScalToU8(truncate=truncate, or_default=or_default)
    OrganizeU8()

    Self.value -> Linearize.vector,value -> ScalToU8.value,value -> OrganizeU8.value,values -> Self.data
    Self.value -> ToVoid.vector,pattern -----------------------------> OrganizeU8.pattern
}

use std/conversion/scalar/i128::ToU16 as ScalToU16
use std/flow/u16::Organize as OrganizeU16

/**
Convert stream of `Vec<i128>` into `Vec<u16>` one.

`Vec<i128>` gets converted into `Vec<u16>`, and the resulting vectors are send through stream in continuity.

As this conversion might be lossy (every possible `i128` value cannot fit into `u16`),
`truncate` allows value to be truncated to fit into a `u16`, and `or_default` set the
value that is assigned when a `i128` is out of range for `u16` and truncation not allowed.

Truncation happens on the binary level, thus: `10010110` (150 if unsigned, -106 if [signed](https://en.wikipedia.org/wiki/Signed_number_representations)) → `0110` (6).
*/
sequence ToU16(var truncate: bool = true, var or_default: u16 = 0)
  input  value: Stream<Vec<i128>>
  output value: Stream<Vec<u16>>
{
    ToVoid()
    Linearize()
    ScalToU16(truncate=truncate, or_default=or_default)
    OrganizeU16()

    Self.value -> Linearize.vector,value -> ScalToU16.value,value -> OrganizeU16.value,values -> Self.data
    Self.value -> ToVoid.vector,pattern -----------------------------> OrganizeU16.pattern
}

use std/conversion/scalar/i128::ToU32 as ScalToU32
use std/flow/u32::Organize as OrganizeU32

/**
Convert stream of `Vec<i128>` into `Vec<u32>` one.

`Vec<i128>` gets converted into `Vec<u32>`, and the resulting vectors are send through stream in continuity.

As this conversion might be lossy (every possible `i128` value cannot fit into `u32`),
`truncate` allows value to be truncated to fit into a `u32`, and `or_default` set the
value that is assigned when a `i128` is out of range for `u32` and truncation not allowed.

Truncation happens on the binary level, thus: `10010110` (150 if unsigned, -106 if [signed](https://en.wikipedia.org/wiki/Signed_number_representations)) → `0110` (6).
*/
sequence ToU32(var truncate: bool = true, var or_default: u32 = 0)
  input  value: Stream<Vec<i128>>
  output value: Stream<Vec<u32>>
{
    ToVoid()
    Linearize()
    ScalToU32(truncate=truncate, or_default=or_default)
    OrganizeU32()

    Self.value -> Linearize.vector,value -> ScalToU32.value,value -> OrganizeU32.value,values -> Self.data
    Self.value -> ToVoid.vector,pattern -----------------------------> OrganizeU32.pattern
}

use std/conversion/scalar/i128::ToU64 as ScalToU64
use std/flow/u64::Organize as OrganizeU64

/**
Convert stream of `Vec<i128>` into `Vec<u64>` one.

`Vec<i128>` gets converted into `Vec<u64>`, and the resulting vectors are send through stream in continuity.

As this conversion might be lossy (every possible `i128` value cannot fit into `u64`),
`truncate` allows value to be truncated to fit into a `u64`, and `or_default` set the
value that is assigned when a `i128` is out of range for `u64` and truncation not allowed.

Truncation happens on the binary level, thus: `10010110` (150 if unsigned, -106 if [signed](https://en.wikipedia.org/wiki/Signed_number_representations)) → `0110` (6).
*/
sequence ToU64(var truncate: bool = true, var or_default: u64 = 0)
  input  value: Stream<Vec<i128>>
  output value: Stream<Vec<u64>>
{
    ToVoid()
    Linearize()
    ScalToU64(truncate=truncate, or_default=or_default)
    OrganizeU64()

    Self.value -> Linearize.vector,value -> ScalToU64.value,value -> OrganizeU64.value,values -> Self.data
    Self.value -> ToVoid.vector,pattern -----------------------------> OrganizeU64.pattern
}

use std/conversion/scalar/i128::ToU128 as ScalToU128
use std/flow/u128::Organize as OrganizeU128

/**
Convert stream of `Vec<i128>` into `Vec<u128>` one.

`Vec<i128>` gets converted into `Vec<u128>`, and the resulting vectors are send through stream in continuity.

As this conversion might be lossy (every possible `i128` value cannot fit into `u128`),
`truncate` allows value to be truncated to fit into a `u128`, and `or_default` set the
value that is assigned when a `i128` is out of range for `u128` and truncation not allowed.

Truncation happens on the binary level, thus: `10010110` (150 if unsigned, -106 if [signed](https://en.wikipedia.org/wiki/Signed_number_representations)) → `0110` (6).
*/
sequence ToU128(var truncate: bool = true, var or_default: u128 = 0)
  input  value: Stream<Vec<i128>>
  output value: Stream<Vec<u128>>
{
    ToVoid()
    Linearize()
    ScalToU128(truncate=truncate, or_default=or_default)
    OrganizeU128()

    Self.value -> Linearize.vector,value -> ScalToU128.value,value -> OrganizeU128.value,values -> Self.data
    Self.value -> ToVoid.vector,pattern -----------------------------> OrganizeU128.pattern
}

use std/conversion/scalar/i128::ToI8 as ScalToI8
use std/flow/i8::Organize as OrganizeI8

/**
Convert stream of `Vec<i128>` into `Vec<i8>` one.

`Vec<i128>` gets converted into `Vec<i8>`, and the resulting vectors are send through stream in continuity.

As this conversion might be lossy (every possible `i128` value cannot fit into `i8`),
`truncate` allows value to be truncated to fit into a `i8`, and `or_default` set the
value that is assigned when a `i128` is out of range for `i8` and truncation not allowed.

Truncation happens on the binary level, thus: `10010110` (150 if unsigned, -106 if [signed](https://en.wikipedia.org/wiki/Signed_number_representations)) → `0110` (6).
*/
sequence ToI8(var truncate: bool = true, var or_default: i8 = 0)
  input  value: Stream<Vec<i128>>
  output value: Stream<Vec<i8>>
{
    ToVoid()
    Linearize()
    ScalToI8(truncate=truncate, or_default=or_default)
    OrganizeI8()

    Self.value -> Linearize.vector,value -> ScalToI8.value,value -> OrganizeI8.value,values -> Self.data
    Self.value -> ToVoid.vector,pattern -----------------------------> OrganizeI8.pattern
}

use std/conversion/scalar/i128::ToI16 as ScalToI16
use std/flow/i16::Organize as OrganizeI16

/**
Convert stream of `Vec<i128>` into `Vec<i16>` one.

`Vec<i128>` gets converted into `Vec<i16>`, and the resulting vectors are send through stream in continuity.

As this conversion might be lossy (every possible `i128` value cannot fit into `i16`),
`truncate` allows value to be truncated to fit into a `i16`, and `or_default` set the
value that is assigned when a `i128` is out of range for `i16` and truncation not allowed.

Truncation happens on the binary level, thus: `10010110` (150 if unsigned, -106 if [signed](https://en.wikipedia.org/wiki/Signed_number_representations)) → `0110` (6).
*/
sequence ToI16(var truncate: bool = true, var or_default: i16 = 0)
  input  value: Stream<Vec<i128>>
  output value: Stream<Vec<i16>>
{
    ToVoid()
    Linearize()
    ScalToI16(truncate=truncate, or_default=or_default)
    OrganizeI16()

    Self.value -> Linearize.vector,value -> ScalToI16.value,value -> OrganizeI16.value,values -> Self.data
    Self.value -> ToVoid.vector,pattern -----------------------------> OrganizeI16.pattern
}

use std/conversion/scalar/i128::ToI32 as ScalToI32
use std/flow/i32::Organize as OrganizeI32

/**
Convert stream of `Vec<i128>` into `Vec<i32>` one.

`Vec<i128>` gets converted into `Vec<i32>`, and the resulting vectors are send through stream in continuity.

As this conversion might be lossy (every possible `i128` value cannot fit into `i32`),
`truncate` allows value to be truncated to fit into a `i32`, and `or_default` set the
value that is assigned when a `i128` is out of range for `i32` and truncation not allowed.

Truncation happens on the binary level, thus: `10010110` (150 if unsigned, -106 if [signed](https://en.wikipedia.org/wiki/Signed_number_representations)) → `0110` (6).
*/
sequence ToI32(var truncate: bool = true, var or_default: i32 = 0)
  input  value: Stream<Vec<i128>>
  output value: Stream<Vec<i32>>
{
    ToVoid()
    Linearize()
    ScalToI32(truncate=truncate, or_default=or_default)
    OrganizeI32()

    Self.value -> Linearize.vector,value -> ScalToI32.value,value -> OrganizeI32.value,values -> Self.data
    Self.value -> ToVoid.vector,pattern -----------------------------> OrganizeI32.pattern
}

use std/conversion/scalar/i128::ToI64 as ScalToI64
use std/flow/i64::Organize as OrganizeI64

/**
Convert stream of `Vec<i128>` into `Vec<i64>` one.

`Vec<i128>` gets converted into `Vec<i64>`, and the resulting vectors are send through stream in continuity.

As this conversion might be lossy (every possible `i128` value cannot fit into `i64`),
`truncate` allows value to be truncated to fit into a `i64`, and `or_default` set the
value that is assigned when a `i128` is out of range for `i64` and truncation not allowed.

Truncation happens on the binary level, thus: `10010110` (150 if unsigned, -106 if [signed](https://en.wikipedia.org/wiki/Signed_number_representations)) → `0110` (6).
*/
sequence ToI64(var truncate: bool = true, var or_default: i64 = 0)
  input  value: Stream<Vec<i128>>
  output value: Stream<Vec<i64>>
{
    ToVoid()
    Linearize()
    ScalToI64(truncate=truncate, or_default=or_default)
    OrganizeI64()

    Self.value -> Linearize.vector,value -> ScalToI64.value,value -> OrganizeI64.value,values -> Self.data
    Self.value -> ToVoid.vector,pattern -----------------------------> OrganizeI64.pattern
}

