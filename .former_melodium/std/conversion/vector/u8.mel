use std/flow/u8::Linearize

use core/conversion/vector::U8ToVoid

/**
Convert stream of `Vec<u8>` into `Vec<void>` one.

This conversion is useful to extract pattern from a stream of vectors and work on it.

*/
sequence ToVoid()
  input  vector:  Stream<Vec<u8>>
  output pattern: Stream<Vec<void>>
{
    U8ToVoid()

    Self.vector -> U8ToVoid.vector,pattern -> Self.pattern
}

use std/conversion/scalar/u8::ToString as ScalToString
use std/flow/string::Organize as OrganizeString

/**
Convert stream of `Vec<u8>` into `Vec<string>` one.

`Vec<u8>` gets converted into `Vec<string>`, and the resulting vectors are send through stream in continuity.
*/
sequence ToString()
  input  value: Stream<Vec<u8>>
  output value: Stream<Vec<string>>
{
    ToVoid()
    Linearize()
    ScalToString()
    OrganizeString()

    Self.value -> Linearize.vector,value -> ScalToString.value,value -> OrganizeString.value,values -> Self.value
    Self.value -> ToVoid.vector,pattern ------------------------------> OrganizeString.pattern
}

use std/conversion/scalar/u8::ToU16 as ScalToU16
use std/flow/u16::Organize as OrganizeU16

/**
Convert stream of `Vec<u8>` into `Vec<u16>` one.

`Vec<u8>` gets converted into `Vec<u16>`, and the resulting vectors are send through stream in continuity.
This conversion is lossless, `u8` values can all fit into `u16`.
*/
sequence ToU16()
  input  value: Stream<Vec<u8>>
  output value: Stream<Vec<u16>>
{
    ToVoid()
    Linearize()
    ScalToU16()
    OrganizeU16()

    Self.value -> Linearize.vector,value -> ScalToU16.value,value -> OrganizeU16.value,values -> Self.data
    Self.value -> ToVoid.vector,pattern -----------------------------> OrganizeU16.pattern
}

use std/conversion/scalar/u8::ToU32 as ScalToU32
use std/flow/u32::Organize as OrganizeU32

/**
Convert stream of `Vec<u8>` into `Vec<u32>` one.

`Vec<u8>` gets converted into `Vec<u32>`, and the resulting vectors are send through stream in continuity.
This conversion is lossless, `u8` values can all fit into `u32`.
*/
sequence ToU32()
  input  value: Stream<Vec<u8>>
  output value: Stream<Vec<u32>>
{
    ToVoid()
    Linearize()
    ScalToU32()
    OrganizeU32()

    Self.value -> Linearize.vector,value -> ScalToU32.value,value -> OrganizeU32.value,values -> Self.data
    Self.value -> ToVoid.vector,pattern -----------------------------> OrganizeU32.pattern
}

use std/conversion/scalar/u8::ToU64 as ScalToU64
use std/flow/u64::Organize as OrganizeU64

/**
Convert stream of `Vec<u8>` into `Vec<u64>` one.

`Vec<u8>` gets converted into `Vec<u64>`, and the resulting vectors are send through stream in continuity.
This conversion is lossless, `u8` values can all fit into `u64`.
*/
sequence ToU64()
  input  value: Stream<Vec<u8>>
  output value: Stream<Vec<u64>>
{
    ToVoid()
    Linearize()
    ScalToU64()
    OrganizeU64()

    Self.value -> Linearize.vector,value -> ScalToU64.value,value -> OrganizeU64.value,values -> Self.data
    Self.value -> ToVoid.vector,pattern -----------------------------> OrganizeU64.pattern
}

use std/conversion/scalar/u8::ToU128 as ScalToU128
use std/flow/u128::Organize as OrganizeU128

/**
Convert stream of `Vec<u8>` into `Vec<u128>` one.

`Vec<u8>` gets converted into `Vec<u128>`, and the resulting vectors are send through stream in continuity.
This conversion is lossless, `u8` values can all fit into `u128`.
*/
sequence ToU128()
  input  value: Stream<Vec<u8>>
  output value: Stream<Vec<u128>>
{
    ToVoid()
    Linearize()
    ScalToU128()
    OrganizeU128()

    Self.value -> Linearize.vector,value -> ScalToU128.value,value -> OrganizeU128.value,values -> Self.data
    Self.value -> ToVoid.vector,pattern -----------------------------> OrganizeU128.pattern
}

use std/conversion/scalar/u8::ToI16 as ScalToI16
use std/flow/i16::Organize as OrganizeI16

/**
Convert stream of `Vec<u8>` into `Vec<i16>` one.

`Vec<u8>` gets converted into `Vec<i16>`, and the resulting vectors are send through stream in continuity.
This conversion is lossless, `u8` values can all fit into `i16`.
*/
sequence ToI16()
  input  value: Stream<Vec<u8>>
  output value: Stream<Vec<i16>>
{
    ToVoid()
    Linearize()
    ScalToI16()
    OrganizeI16()

    Self.value -> Linearize.vector,value -> ScalToI16.value,value -> OrganizeI16.value,values -> Self.data
    Self.value -> ToVoid.vector,pattern -----------------------------> OrganizeI16.pattern
}

use std/conversion/scalar/u8::ToI32 as ScalToI32
use std/flow/i32::Organize as OrganizeI32

/**
Convert stream of `Vec<u8>` into `Vec<i32>` one.

`Vec<u8>` gets converted into `Vec<i32>`, and the resulting vectors are send through stream in continuity.
This conversion is lossless, `u8` values can all fit into `i32`.
*/
sequence ToI32()
  input  value: Stream<Vec<u8>>
  output value: Stream<Vec<i32>>
{
    ToVoid()
    Linearize()
    ScalToI32()
    OrganizeI32()

    Self.value -> Linearize.vector,value -> ScalToI32.value,value -> OrganizeI32.value,values -> Self.data
    Self.value -> ToVoid.vector,pattern -----------------------------> OrganizeI32.pattern
}

use std/conversion/scalar/u8::ToI64 as ScalToI64
use std/flow/i64::Organize as OrganizeI64

/**
Convert stream of `Vec<u8>` into `Vec<i64>` one.

`Vec<u8>` gets converted into `Vec<i64>`, and the resulting vectors are send through stream in continuity.
This conversion is lossless, `u8` values can all fit into `i64`.
*/
sequence ToI64()
  input  value: Stream<Vec<u8>>
  output value: Stream<Vec<i64>>
{
    ToVoid()
    Linearize()
    ScalToI64()
    OrganizeI64()

    Self.value -> Linearize.vector,value -> ScalToI64.value,value -> OrganizeI64.value,values -> Self.data
    Self.value -> ToVoid.vector,pattern -----------------------------> OrganizeI64.pattern
}

use std/conversion/scalar/u8::ToI128 as ScalToI128
use std/flow/i128::Organize as OrganizeI128

/**
Convert stream of `Vec<u8>` into `Vec<i128>` one.

`Vec<u8>` gets converted into `Vec<i128>`, and the resulting vectors are send through stream in continuity.
This conversion is lossless, `u8` values can all fit into `i128`.
*/
sequence ToI128()
  input  value: Stream<Vec<u8>>
  output value: Stream<Vec<i128>>
{
    ToVoid()
    Linearize()
    ScalToI128()
    OrganizeI128()

    Self.value -> Linearize.vector,value -> ScalToI128.value,value -> OrganizeI128.value,values -> Self.data
    Self.value -> ToVoid.vector,pattern -----------------------------> OrganizeI128.pattern
}

use std/conversion/scalar/u8::ToF32 as ScalToF32
use std/flow/f32::Organize as OrganizeF32

/**
Convert stream of `Vec<u8>` into `Vec<f32>` one.

`Vec<u8>` gets converted into `Vec<f32>`, and the resulting vectors are send through stream in continuity.
This conversion is lossless, `u8` values can all fit into `f32`.
*/
sequence ToF32()
  input  value: Stream<Vec<u8>>
  output value: Stream<Vec<f32>>
{
    ToVoid()
    Linearize()
    ScalToF32()
    OrganizeF32()

    Self.value -> Linearize.vector,value -> ScalToF32.value,value -> OrganizeF32.value,values -> Self.data
    Self.value -> ToVoid.vector,pattern -----------------------------> OrganizeF32.pattern
}

use std/conversion/scalar/u8::ToF64 as ScalToF64
use std/flow/f64::Organize as OrganizeF64

/**
Convert stream of `Vec<u8>` into `Vec<f64>` one.

`Vec<u8>` gets converted into `Vec<f64>`, and the resulting vectors are send through stream in continuity.
This conversion is lossless, `u8` values can all fit into `f64`.
*/
sequence ToF64()
  input  value: Stream<Vec<u8>>
  output value: Stream<Vec<f64>>
{
    ToVoid()
    Linearize()
    ScalToF64()
    OrganizeF64()

    Self.value -> Linearize.vector,value -> ScalToF64.value,value -> OrganizeF64.value,values -> Self.data
    Self.value -> ToVoid.vector,pattern -----------------------------> OrganizeF64.pattern
}

use std/conversion/scalar/u8::ToI8 as ScalToI8
use std/flow/i8::Organize as OrganizeI8

/**
Convert stream of `Vec<u8>` into `Vec<i8>` one.

`Vec<u8>` gets converted into `Vec<i8>`, and the resulting vectors are send through stream in continuity.

As this conversion might be lossy (every possible `u8` value cannot fit into `i8`),
`truncate` allows value to be truncated to fit into a `i8`, and `or_default` set the
value that is assigned when a `u8` is out of range for `i8` and truncation not allowed.

Truncation happens on the binary level, thus: `10010110` (150 if unsigned, -106 if [signed](https://en.wikipedia.org/wiki/Signed_number_representations)) → `0110` (6).
*/
sequence ToI8(var truncate: bool = true, var or_default: i8 = 0)
  input  value: Stream<Vec<u8>>
  output value: Stream<Vec<i8>>
{
    ToVoid()
    Linearize()
    ScalToI8(truncate=truncate, or_default=or_default)
    OrganizeI8()

    Self.value -> Linearize.vector,value -> ScalToI8.value,value -> OrganizeI8.value,values -> Self.data
    Self.value -> ToVoid.vector,pattern -----------------------------> OrganizeI8.pattern
}

