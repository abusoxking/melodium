use core/conversion/scalar::I16ToVoid

/**
Turns `i16` stream into `void` one.

Send one `iter` per input `value` received.
*/
sequence ToVoid()
  input value: Stream<i16>
  output iter: Stream<void>
{
    I16ToVoid()

    Self.value -> I16ToVoid.value,iter -> Self.iter
}

use core/conversion/scalar::I16ToByte

/**
Convert stream of `i16` into `Vec<byte>`.

`i16` gets converted into `Vec<byte>`, each vector contains the bytes of the former scalar `i16` it represents.
*/
sequence ToByte()
  input  value: Stream<i16>
  output data:  Stream<Vec<byte>>
{
    I16ToByte()

    Self.value -> I16ToByte.value,data -> Self.data
}

use core/conversion/scalar::I16FromByte

/**
Convert stream of `Vec<byte>` into `i16`.

Each received `byte` vector try to be converted into `i16`, and if valid is sent as `value`. If the incoming vector 
is not valid for representing a `i16` (i.e. not right size or invalid coding) it is refused and sent through `reject`.
*/
sequence FromByte()
  input  data:   Stream<Vec<byte>>
  output value:  Stream<i16>
  output reject: Stream<Vec<byte>>
{
    I16FromByte()

    Self.data -> I16FromByte.data,value -> Self.value
                 I16FromByte.reject -----> Self.reject
}

use core/conversion/scalar::I16ToString

/**
Convert stream of `i16` into `string`.

`i16` gets converted into `string`, and the resulting strings are send through stream in continuity.
*/
sequence ToString()
  input  value: Stream<i16>
  output value: Stream<string>
{
    I16ToString()

    Self.value -> I16ToString.value,value -> Self.value
}

use core/cast/scalar::I16ToI32

/**
Convert stream of `i16` into `i32`.

This conversion is lossless, `i16` values can all fit into `i32`.
*/
sequence ToI32()
  input  value: Stream<i16>
  output value: Stream<i32>
{
    I16ToI32()

    Self.value -> I16ToI32.value,value -> Self.value
}


use core/cast/scalar::I16ToI64

/**
Convert stream of `i16` into `i64`.

This conversion is lossless, `i16` values can all fit into `i64`.
*/
sequence ToI64()
  input  value: Stream<i16>
  output value: Stream<i64>
{
    I16ToI64()

    Self.value -> I16ToI64.value,value -> Self.value
}


use core/cast/scalar::I16ToI128

/**
Convert stream of `i16` into `i128`.

This conversion is lossless, `i16` values can all fit into `i128`.
*/
sequence ToI128()
  input  value: Stream<i16>
  output value: Stream<i128>
{
    I16ToI128()

    Self.value -> I16ToI128.value,value -> Self.value
}


use core/cast/scalar::I16ToF32

/**
Convert stream of `i16` into `f32`.

This conversion is lossless, `i16` values can all fit into `f32`.
*/
sequence ToF32()
  input  value: Stream<i16>
  output value: Stream<f32>
{
    I16ToF32()

    Self.value -> I16ToF32.value,value -> Self.value
}


use core/cast/scalar::I16ToF64

/**
Convert stream of `i16` into `f64`.

This conversion is lossless, `i16` values can all fit into `f64`.
*/
sequence ToF64()
  input  value: Stream<i16>
  output value: Stream<f64>
{
    I16ToF64()

    Self.value -> I16ToF64.value,value -> Self.value
}


use core/cast/scalar::CastScalarI16ToU8

/**
Convert stream of `i16` into `u8`.

As this conversion might be lossy (every possible `i16` value cannot fit into `u8`),
`truncate` allows value to be truncated to fit into a `u8`, and `or_default` set the
value that is assigned when a `i16` is out of range for `u8` and truncation not allowed.

Truncation happens on the binary level, thus: `10010110` (150 if unsigned, -106 if [signed](https://en.wikipedia.org/wiki/Signed_number_representations)) → `0110` (6).
*/
sequence ToU8(var truncate: bool = true, var or_default: u8 = 0)
  input  value: Stream<i16>
  output value: Stream<u8>
{
    CastScalarI16ToU8(truncate=truncate, or_default=or_default)

    Self.value -> CastScalarI16ToU8.value,value -> Self.value
}


use core/cast/scalar::CastScalarI16ToU16

/**
Convert stream of `i16` into `u16`.

As this conversion might be lossy (every possible `i16` value cannot fit into `u16`),
`truncate` allows value to be truncated to fit into a `u16`, and `or_default` set the
value that is assigned when a `i16` is out of range for `u16` and truncation not allowed.

Truncation happens on the binary level, thus: `10010110` (150 if unsigned, -106 if [signed](https://en.wikipedia.org/wiki/Signed_number_representations)) → `0110` (6).
*/
sequence ToU16(var truncate: bool = true, var or_default: u16 = 0)
  input  value: Stream<i16>
  output value: Stream<u16>
{
    CastScalarI16ToU16(truncate=truncate, or_default=or_default)

    Self.value -> CastScalarI16ToU16.value,value -> Self.value
}


use core/cast/scalar::CastScalarI16ToU32

/**
Convert stream of `i16` into `u32`.

As this conversion might be lossy (every possible `i16` value cannot fit into `u32`),
`truncate` allows value to be truncated to fit into a `u32`, and `or_default` set the
value that is assigned when a `i16` is out of range for `u32` and truncation not allowed.

Truncation happens on the binary level, thus: `10010110` (150 if unsigned, -106 if [signed](https://en.wikipedia.org/wiki/Signed_number_representations)) → `0110` (6).
*/
sequence ToU32(var truncate: bool = true, var or_default: u32 = 0)
  input  value: Stream<i16>
  output value: Stream<u32>
{
    CastScalarI16ToU32(truncate=truncate, or_default=or_default)

    Self.value -> CastScalarI16ToU32.value,value -> Self.value
}


use core/cast/scalar::CastScalarI16ToU64

/**
Convert stream of `i16` into `u64`.

As this conversion might be lossy (every possible `i16` value cannot fit into `u64`),
`truncate` allows value to be truncated to fit into a `u64`, and `or_default` set the
value that is assigned when a `i16` is out of range for `u64` and truncation not allowed.

Truncation happens on the binary level, thus: `10010110` (150 if unsigned, -106 if [signed](https://en.wikipedia.org/wiki/Signed_number_representations)) → `0110` (6).
*/
sequence ToU64(var truncate: bool = true, var or_default: u64 = 0)
  input  value: Stream<i16>
  output value: Stream<u64>
{
    CastScalarI16ToU64(truncate=truncate, or_default=or_default)

    Self.value -> CastScalarI16ToU64.value,value -> Self.value
}


use core/cast/scalar::CastScalarI16ToU128

/**
Convert stream of `i16` into `u128`.

As this conversion might be lossy (every possible `i16` value cannot fit into `u128`),
`truncate` allows value to be truncated to fit into a `u128`, and `or_default` set the
value that is assigned when a `i16` is out of range for `u128` and truncation not allowed.

Truncation happens on the binary level, thus: `10010110` (150 if unsigned, -106 if [signed](https://en.wikipedia.org/wiki/Signed_number_representations)) → `0110` (6).
*/
sequence ToU128(var truncate: bool = true, var or_default: u128 = 0)
  input  value: Stream<i16>
  output value: Stream<u128>
{
    CastScalarI16ToU128(truncate=truncate, or_default=or_default)

    Self.value -> CastScalarI16ToU128.value,value -> Self.value
}


use core/cast/scalar::CastScalarI16ToI8

/**
Convert stream of `i16` into `i8`.

As this conversion might be lossy (every possible `i16` value cannot fit into `i8`),
`truncate` allows value to be truncated to fit into a `i8`, and `or_default` set the
value that is assigned when a `i16` is out of range for `i8` and truncation not allowed.

Truncation happens on the binary level, thus: `10010110` (150 if unsigned, -106 if [signed](https://en.wikipedia.org/wiki/Signed_number_representations)) → `0110` (6).
*/
sequence ToI8(var truncate: bool = true, var or_default: i8 = 0)
  input  value: Stream<i16>
  output value: Stream<i8>
{
    CastScalarI16ToI8(truncate=truncate, or_default=or_default)

    Self.value -> CastScalarI16ToI8.value,value -> Self.value
}


