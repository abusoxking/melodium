use core/conversion/scalar::U8ToVoid

/**
Turns `u8` stream into `void` one.

Send one `iter` per input `value` received.
*/
sequence ToVoid()
  input value: Stream<u8>
  output iter: Stream<void>
{
    U8ToVoid()

    Self.value -> U8ToVoid.value,iter -> Self.iter
}

use core/conversion/scalar::U8ToByte

/**
Convert stream of `u8` into `Vec<byte>`.

`u8` gets converted into `Vec<byte>`, each vector contains the bytes of the former scalar `u8` it represents.
*/
sequence ToByte()
  input  value: Stream<u8>
  output data:  Stream<Vec<byte>>
{
    U8ToByte()

    Self.value -> U8ToByte.value,data -> Self.data
}

use core/conversion/scalar::U8FromByte

/**
Convert stream of `Vec<byte>` into `u8`.

Each received `byte` vector try to be converted into `u8`, and if valid is sent as `value`. If the incoming vector 
is not valid for representing a `u8` (i.e. not right size or invalid coding) it is refused and sent through `reject`.
*/
sequence FromByte()
  input  data:   Stream<Vec<byte>>
  output value:  Stream<u8>
  output reject: Stream<Vec<byte>>
{
    U8FromByte()

    Self.data -> U8FromByte.data,value -> Self.value
                 U8FromByte.reject -----> Self.reject
}

use core/conversion/scalar::U8ToString

/**
Convert stream of `u8` into `string`.

`u8` gets converted into `string`, and the resulting strings are send through stream in continuity.
*/
sequence ToString()
  input  value: Stream<u8>
  output value: Stream<string>
{
    U8ToString()

    Self.value -> U8ToString.value,value -> Self.value
}

use core/cast/scalar::U8ToU16

/**
Convert stream of `u8` into `u16`.

This conversion is lossless, `u8` values can all fit into `u16`.
*/
sequence ToU16()
  input  value: Stream<u8>
  output value: Stream<u16>
{
    U8ToU16()

    Self.value -> U8ToU16.value,value -> Self.value
}


use core/cast/scalar::U8ToU32

/**
Convert stream of `u8` into `u32`.

This conversion is lossless, `u8` values can all fit into `u32`.
*/
sequence ToU32()
  input  value: Stream<u8>
  output value: Stream<u32>
{
    U8ToU32()

    Self.value -> U8ToU32.value,value -> Self.value
}


use core/cast/scalar::U8ToU64

/**
Convert stream of `u8` into `u64`.

This conversion is lossless, `u8` values can all fit into `u64`.
*/
sequence ToU64()
  input  value: Stream<u8>
  output value: Stream<u64>
{
    U8ToU64()

    Self.value -> U8ToU64.value,value -> Self.value
}


use core/cast/scalar::U8ToU128

/**
Convert stream of `u8` into `u128`.

This conversion is lossless, `u8` values can all fit into `u128`.
*/
sequence ToU128()
  input  value: Stream<u8>
  output value: Stream<u128>
{
    U8ToU128()

    Self.value -> U8ToU128.value,value -> Self.value
}


use core/cast/scalar::U8ToI16

/**
Convert stream of `u8` into `i16`.

This conversion is lossless, `u8` values can all fit into `i16`.
*/
sequence ToI16()
  input  value: Stream<u8>
  output value: Stream<i16>
{
    U8ToI16()

    Self.value -> U8ToI16.value,value -> Self.value
}


use core/cast/scalar::U8ToI32

/**
Convert stream of `u8` into `i32`.

This conversion is lossless, `u8` values can all fit into `i32`.
*/
sequence ToI32()
  input  value: Stream<u8>
  output value: Stream<i32>
{
    U8ToI32()

    Self.value -> U8ToI32.value,value -> Self.value
}


use core/cast/scalar::U8ToI64

/**
Convert stream of `u8` into `i64`.

This conversion is lossless, `u8` values can all fit into `i64`.
*/
sequence ToI64()
  input  value: Stream<u8>
  output value: Stream<i64>
{
    U8ToI64()

    Self.value -> U8ToI64.value,value -> Self.value
}


use core/cast/scalar::U8ToI128

/**
Convert stream of `u8` into `i128`.

This conversion is lossless, `u8` values can all fit into `i128`.
*/
sequence ToI128()
  input  value: Stream<u8>
  output value: Stream<i128>
{
    U8ToI128()

    Self.value -> U8ToI128.value,value -> Self.value
}


use core/cast/scalar::U8ToF32

/**
Convert stream of `u8` into `f32`.

This conversion is lossless, `u8` values can all fit into `f32`.
*/
sequence ToF32()
  input  value: Stream<u8>
  output value: Stream<f32>
{
    U8ToF32()

    Self.value -> U8ToF32.value,value -> Self.value
}


use core/cast/scalar::U8ToF64

/**
Convert stream of `u8` into `f64`.

This conversion is lossless, `u8` values can all fit into `f64`.
*/
sequence ToF64()
  input  value: Stream<u8>
  output value: Stream<f64>
{
    U8ToF64()

    Self.value -> U8ToF64.value,value -> Self.value
}


use core/cast/scalar::CastScalarU8ToI8

/**
Convert stream of `u8` into `i8`.

As this conversion might be lossy (every possible `u8` value cannot fit into `i8`),
`truncate` allows value to be truncated to fit into a `i8`, and `or_default` set the
value that is assigned when a `u8` is out of range for `i8` and truncation not allowed.

Truncation happens on the binary level, thus: `10010110` (150 if unsigned, -106 if [signed](https://en.wikipedia.org/wiki/Signed_number_representations)) → `0110` (6).
*/
sequence ToI8(var truncate: bool = true, var or_default: i8 = 0)
  input  value: Stream<u8>
  output value: Stream<i8>
{
    CastScalarU8ToI8(truncate=truncate, or_default=or_default)

    Self.value -> CastScalarU8ToI8.value,value -> Self.value
}


