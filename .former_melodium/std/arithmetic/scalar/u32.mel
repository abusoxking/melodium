use core/arithmetic/scalar::StaticAddU32

/**
Add a static value to `u32`.

Every number passed through the stream get `add` added.
*/
sequence StaticAdd(var add: u32 = 0)
  input  value: Stream<u32>
  output value: Stream<u32>
{
    StaticAddU32(add=add)

    Self.value -> StaticAddU32.value,value -> Self.value
}

use core/arithmetic/scalar::AddU32

/**
Add values from two streams of `u32`.

Values passed through a & b are added and send in sum.

*/
sequence Add()
  input  a:   Stream<u32>
  input  b:   Stream<u32>
  output sum: Stream<u32>
{
    AddU32()

    Self.a -> AddU32.a
    Self.b -> AddU32.b
    
    AddU32.sum -> Self.sum
}

use core/arithmetic/scalar::StaticSubU32 

/**
Substract a static value to `u32`.

Every number passed through the stream get `sub` substracted.

*/
sequence StaticSub(var sub: u32 = 0)
  input  value: Stream<u32>
  output value: Stream<u32>
{
    StaticSubU32(sub=sub)

    Self.value -> StaticSubU32.value,value -> Self.value
}


use core/arithmetic/scalar::SubU32 

/**
Substract values from two streams of `u32`.

Every `a` number passed through the stream get `b` substracted.

*/
sequence Sub()
  input  a:          Stream<u32>
  input  b:          Stream<u32>
  output difference: Stream<u32>
{
    SubU32()

    Self.a -> SubU32.a
    Self.b -> SubU32.b
    
    SubU32.difference -> Self.difference
}


use core/arithmetic/scalar::StaticMultU32 

/**
Multiply `u32` by static value.

Every number passed through the stream is multiplied by `factor`.

*/
sequence StaticMult(var factor: u32 = 0)
  input  value: Stream<u32>
  output value: Stream<u32>
{
    StaticMultU32(factor=factor)

    Self.value -> StaticMultU32.value,value -> Self.value
}


use core/arithmetic/scalar::MultU32 

/**
Multiply values from two streams of `u32`.

Every `a` number passed through the stream is multiplied by `b`.

*/
sequence Mult()
  input  a:       Stream<u32>
  input  b:       Stream<u32>
  output product: Stream<u32>
{
    MultU32()

    Self.a -> MultU32.a
    Self.b -> MultU32.b
    
    MultU32.product -> Self.product
}


use core/arithmetic/scalar::StaticDivU32 

/**
Divide a stream of `u32` by a static value.

Every number passed through the stream is divided by `divisor`.

*/
sequence StaticDiv(var divisor: u32 = 0)
  input  value: Stream<u32>
  output value: Stream<u32>
{
    StaticDivU32(divisor=divisor)

    Self.value -> StaticDivU32.value,value -> Self.value
}


use core/arithmetic/scalar::DivU32 

/**
Divide values from two streams of `u32`.

Every `a` number passed through the stream is divided by `b`.

*/
sequence Div()
  input  a:        Stream<u32>
  input  b:        Stream<u32>
  output quotient: Stream<u32>
{
    DivU32()

    Self.a -> DivU32.a
    Self.b -> DivU32.b
    
    DivU32.quotient -> Self.quotient
}


use core/arithmetic/scalar::StaticRemU32 

/**
Give the remainder of a stream of `u32` divided by a static value.

Every number passed through the stream is divided by `divisor` and the remainder is outputted.

*/
sequence StaticRem(var divisor: u32 = 0)
  input  value: Stream<u32>
  output value: Stream<u32>
{
    StaticRemU32(divisor=divisor)

    Self.value -> StaticRemU32.value,value -> Self.value
}


use core/arithmetic/scalar::RemU32 

/**
Give the remainder of the division from two streams of `u32`.

Every `a` number passed through the stream is divided by `b` and the remainder is outputted.

*/
sequence Rem()
  input  a:         Stream<u32>
  input  b:         Stream<u32>
  output remainder: Stream<u32>
{
    RemU32()

    Self.a -> RemU32.a
    Self.b -> RemU32.b
    
    RemU32.remainder -> Self.remainder
}




use core/arithmetic/scalar::StaticPowU32 

/**
Elevates `u32` to the power of a static value.

Every number passed through the stream get elevated to the power of `exponent`.
*/
sequence StaticPow(var exponent: u32 = 0)
  input  value: Stream<u32>
  output value: Stream<u32>
{
    StaticPowU32(exponent=exponent)

    Self.value -> StaticPowU32.value,value -> Self.value
}


use core/arithmetic/scalar::PowU32 

/**
Elevates values from a stream of `u32` to the power of another one.

Values passed through `base` are elevated to the power of `exponent`.

*/
sequence Pow()
  input  base:     Stream<u32>
  input  exponent: Stream<u32>
  output power:    Stream<u32>
{
    PowU32()

    Self.base -----> PowU32.base
    Self.exponent -> PowU32.exponent
    
    PowU32.power -> Self.power
}
