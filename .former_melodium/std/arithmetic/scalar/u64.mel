use core/arithmetic/scalar::StaticAddU64

/**
Add a static value to `u64`.

Every number passed through the stream get `add` added.
*/
sequence StaticAdd(var add: u64 = 0)
  input  value: Stream<u64>
  output value: Stream<u64>
{
    StaticAddU64(add=add)

    Self.value -> StaticAddU64.value,value -> Self.value
}

use core/arithmetic/scalar::AddU64

/**
Add values from two streams of `u64`.

Values passed through a & b are added and send in sum.

*/
sequence Add()
  input  a:   Stream<u64>
  input  b:   Stream<u64>
  output sum: Stream<u64>
{
    AddU64()

    Self.a -> AddU64.a
    Self.b -> AddU64.b
    
    AddU64.sum -> Self.sum
}

use core/arithmetic/scalar::StaticSubU64 

/**
Substract a static value to `u64`.

Every number passed through the stream get `sub` substracted.

*/
sequence StaticSub(var sub: u64 = 0)
  input  value: Stream<u64>
  output value: Stream<u64>
{
    StaticSubU64(sub=sub)

    Self.value -> StaticSubU64.value,value -> Self.value
}


use core/arithmetic/scalar::SubU64 

/**
Substract values from two streams of `u64`.

Every `a` number passed through the stream get `b` substracted.

*/
sequence Sub()
  input  a:          Stream<u64>
  input  b:          Stream<u64>
  output difference: Stream<u64>
{
    SubU64()

    Self.a -> SubU64.a
    Self.b -> SubU64.b
    
    SubU64.difference -> Self.difference
}


use core/arithmetic/scalar::StaticMultU64 

/**
Multiply `u64` by static value.

Every number passed through the stream is multiplied by `factor`.

*/
sequence StaticMult(var factor: u64 = 0)
  input  value: Stream<u64>
  output value: Stream<u64>
{
    StaticMultU64(factor=factor)

    Self.value -> StaticMultU64.value,value -> Self.value
}


use core/arithmetic/scalar::MultU64 

/**
Multiply values from two streams of `u64`.

Every `a` number passed through the stream is multiplied by `b`.

*/
sequence Mult()
  input  a:       Stream<u64>
  input  b:       Stream<u64>
  output product: Stream<u64>
{
    MultU64()

    Self.a -> MultU64.a
    Self.b -> MultU64.b
    
    MultU64.product -> Self.product
}


use core/arithmetic/scalar::StaticDivU64 

/**
Divide a stream of `u64` by a static value.

Every number passed through the stream is divided by `divisor`.

*/
sequence StaticDiv(var divisor: u64 = 0)
  input  value: Stream<u64>
  output value: Stream<u64>
{
    StaticDivU64(divisor=divisor)

    Self.value -> StaticDivU64.value,value -> Self.value
}


use core/arithmetic/scalar::DivU64 

/**
Divide values from two streams of `u64`.

Every `a` number passed through the stream is divided by `b`.

*/
sequence Div()
  input  a:        Stream<u64>
  input  b:        Stream<u64>
  output quotient: Stream<u64>
{
    DivU64()

    Self.a -> DivU64.a
    Self.b -> DivU64.b
    
    DivU64.quotient -> Self.quotient
}


use core/arithmetic/scalar::StaticRemU64 

/**
Give the remainder of a stream of `u64` divided by a static value.

Every number passed through the stream is divided by `divisor` and the remainder is outputted.

*/
sequence StaticRem(var divisor: u64 = 0)
  input  value: Stream<u64>
  output value: Stream<u64>
{
    StaticRemU64(divisor=divisor)

    Self.value -> StaticRemU64.value,value -> Self.value
}


use core/arithmetic/scalar::RemU64 

/**
Give the remainder of the division from two streams of `u64`.

Every `a` number passed through the stream is divided by `b` and the remainder is outputted.

*/
sequence Rem()
  input  a:         Stream<u64>
  input  b:         Stream<u64>
  output remainder: Stream<u64>
{
    RemU64()

    Self.a -> RemU64.a
    Self.b -> RemU64.b
    
    RemU64.remainder -> Self.remainder
}




use core/arithmetic/scalar::StaticPowU64 

/**
Elevates `u64` to the power of a static value.

Every number passed through the stream get elevated to the power of `exponent`.
*/
sequence StaticPow(var exponent: u64 = 0)
  input  value: Stream<u64>
  output value: Stream<u64>
{
    StaticPowU64(exponent=exponent)

    Self.value -> StaticPowU64.value,value -> Self.value
}


use core/arithmetic/scalar::PowU64 

/**
Elevates values from a stream of `u64` to the power of another one.

Values passed through `base` are elevated to the power of `exponent`.

*/
sequence Pow()
  input  base:     Stream<u64>
  input  exponent: Stream<u64>
  output power:    Stream<u64>
{
    PowU64()

    Self.base -----> PowU64.base
    Self.exponent -> PowU64.exponent
    
    PowU64.power -> Self.power
}
