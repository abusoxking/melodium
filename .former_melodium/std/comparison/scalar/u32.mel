use core/comparison/scalar/u32::GreaterEqual as CoreGreaterEqual

/**
Tells whether `a` is greater or equal to `b`.
*/
sequence GreaterEqual()
  input a: Stream<u32>
  input b: Stream<u32>
  output is: Stream<bool>
{
    CoreGreaterEqual()

    Self.a -> CoreGreaterEqual.a
    Self.b -> CoreGreaterEqual.b,is -> Self.is
}


use core/comparison/scalar/u32::LowerEqual as CoreLowerEqual

/**
Tells whether `a` is lower or equal to `b`.
*/
sequence LowerEqual()
  input a: Stream<u32>
  input b: Stream<u32>
  output is: Stream<bool>
{
    CoreLowerEqual()

    Self.a -> CoreLowerEqual.a
    Self.b -> CoreLowerEqual.b,is -> Self.is
}


use core/comparison/scalar/u32::Equal as CoreEqual

/**
Tells whether `a` is equal to `b`.
*/
sequence Equal()
  input a: Stream<u32>
  input b: Stream<u32>
  output is: Stream<bool>
{
    CoreEqual()

    Self.a -> CoreEqual.a
    Self.b -> CoreEqual.b,is -> Self.is
}


use core/comparison/scalar/u32::NotEqual as CoreNotEqual

/**
Tells whether `a` is not equal to `b`.
*/
sequence NotEqual()
  input a: Stream<u32>
  input b: Stream<u32>
  output is: Stream<bool>
{
    CoreNotEqual()

    Self.a -> CoreNotEqual.a
    Self.b -> CoreNotEqual.b,is -> Self.is
}

use core/comparison/scalar/u32::GreaterThan as CoreGreaterThan

/**
Tells whether `a` is strictly greater than `b`.
*/
sequence GreaterThan()
  input a: Stream<u32>
  input b: Stream<u32>
  output is: Stream<bool>
{
    CoreGreaterThan()

    Self.a -> CoreGreaterThan.a
    Self.b -> CoreGreaterThan.b,is -> Self.is
}


use core/comparison/scalar/u32::LowerThan as CoreLowerThan

/**
Tells whether `a` is strictly lower than `b`.
*/
sequence LowerThan()
  input a: Stream<u32>
  input b: Stream<u32>
  output is: Stream<bool>
{
    CoreLowerThan()

    Self.a -> CoreLowerThan.a
    Self.b -> CoreLowerThan.b,is -> Self.is
}


use core/comparison/scalar/u32::Max as CoreMax

/**
Compares and gives the maximum of two values.
*/
sequence Max()
  input a: Stream<u32>
  input b: Stream<u32>
  output value: Stream<u32>
{
    CoreMax()

    Self.a -> CoreMax.a
    Self.b -> CoreMax.b,is -> Self.is
}


use core/comparison/scalar/u32::Min as CoreMin

/**
Compares and gives the minimum of two values.
*/
sequence Min()
  input a: Stream<u32>
  input b: Stream<u32>
  output value: Stream<u32>
{
    CoreMin()

    Self.a -> CoreMin.a
    Self.b -> CoreMin.b,is -> Self.is
}

