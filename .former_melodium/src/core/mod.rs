
//! Contains core Mélodium models and treatments.
//! 
//! # Warning
//! This module is currently being designed.
//! 

pub mod core_collection;

pub mod arithmetic;
pub mod audio;
pub mod cast;
pub mod comparison;
pub mod conversion;
pub mod engine;
pub mod filling;
pub mod filter;
pub mod generation;
pub mod logic;
pub mod flow;
pub mod fs;
pub mod net;
pub mod text;
pub mod trigonometry;

pub mod prelude;
