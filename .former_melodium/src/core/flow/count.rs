
use crate::core::prelude::*;

treatment!(count_scalar,
    core_identifier!("flow","scalar","void";"Count"),
    indoc!(r#"Gives count of elements passing through input stream.

    This count increment one for each elements within the `iter` stream, starting at 1.
    
    ```mermaid
    graph LR
        T("Count()")
        V["🟦 🟦 🟦…"] -->|iter| T
        
        T -->|count| P["1️⃣ 2️⃣ 3️⃣ …"]
    
        style V fill:#ffff,stroke:#ffff
        style P fill:#ffff,stroke:#ffff
    ```"#).to_string(),
    models![],
    treatment_sources![],
    parameters![],
    inputs![
        input!("iter",Scalar,Void,Stream)
    ],
    outputs![
        output!("count",Scalar,U128,Stream)
    ],
    host {
        let input = host.get_input("iter");
        let output = host.get_output("count");

        let mut count = 1u128;

        while let Ok(iter) = input.recv_void().await {

            let mut vec = Vec::with_capacity(iter.len());

            for _ in 0..iter.len() {
                vec.push(count);
                count += 1;
            }

            ok_or_break!(output.send_multiple_u128(vec).await);
        }
    
        ResultStatus::Ok
    }
);

treatment!(count_vector,
    core_identifier!("flow","vector","void";"Count"),
    indoc!(r#"Gives count of elements passing through input stream.

    This count increment one for each elements within the `iter` stream, starting at 1.
    
    ℹ️ This does not count the number of elements present in each vector, see the `Size` sequence instead.
    
    ```mermaid
    graph LR
        T("VecCount()")
        V["［🟦 🟦］［🟦］［🟦 🟦 🟦］…"] -->|iter| T
        
        T -->|count| P["1️⃣ 2️⃣ 3️⃣ …"]
    
        style V fill:#ffff,stroke:#ffff
        style P fill:#ffff,stroke:#ffff
    ```"#).to_string(),
    models![],
    treatment_sources![],
    parameters![],
    inputs![
        input!("iter",Vector,Void,Stream)
    ],
    outputs![
        output!("count",Scalar,U128,Stream)
    ],
    host {
        let input = host.get_input("iter");
        let output = host.get_output("count");

        let mut count = 1u128;

        while let Ok(iter) = input.recv_vec_void().await {

            let mut vec = Vec::with_capacity(iter.len());

            for _ in 0..iter.len() {
                vec.push(count);
                count += 1;
            }

            ok_or_break!(output.send_multiple_u128(vec).await);
        }
    
        ResultStatus::Ok
    }
);

pub fn register(mut c: &mut CollectionPool) {

    count_scalar::register(&mut c);
    count_vector::register(&mut c);
}
