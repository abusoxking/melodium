
use crate::core::prelude::*;

treatment!(char_to_byte,
    core_identifier!("conversion","scalar";"CharToByte"),
    indoc!(r"Convert stream of `char` into `Vec<byte>`.

    Each `char` gets converted into `Vec<byte>`, each vector contains the four bytes of the former scalar `char` it represents.").to_string(),
    models![],
    treatment_sources![],
    parameters![],
    inputs![
        input!("value",Scalar,Char,Stream)
    ],
    outputs![
        output!("data",Vector,Byte,Stream)
    ],
    host {
        let input = host.get_input("value");
        let output = host.get_output("data");
    
        'main: while let Ok(chars) = input.recv_char().await {
    
            for ch in chars {
                ok_or_break!('main, output.send_vec_byte(ch.to_string().as_bytes().to_vec()).await);
            }
        }
    
        ResultStatus::Ok
    }
);
