
//! Describe and manage logic, provide builders to make logic design.
//! 
//! # Warning
//! This module is currently being designed.

pub mod builder;
pub mod collection;
pub mod collection_pool;
pub mod connections;
pub mod contexts;
pub mod descriptor;
pub mod designer;
pub mod error;

