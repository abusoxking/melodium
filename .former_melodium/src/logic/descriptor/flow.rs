
#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug)]
pub enum Flow {
    Block,
    Stream,
}
