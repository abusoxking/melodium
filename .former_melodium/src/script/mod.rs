
//! Allows general script parsing and analysis, building the associated logic model.
//! 
//! The [text](./text/index.html) module handles parsing and primary analysis of textual script.

pub mod base;
pub mod error;
pub mod file;
pub mod instance;
pub mod location;
pub mod path;
pub mod restitution;
pub mod semantic;
pub mod text;
