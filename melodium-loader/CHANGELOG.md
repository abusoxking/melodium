
# Changelog

## Unreleased

- Improving loadgin errors reporting.
- Refactoring loading process.

## [v0.6.2]

- Fixing search path.

## [v0.6.1]

- Removing Collection return at each load operation.
- Adding new, default, and extend functions to LoadingConfig.

## [v0.6.0]

First release.
